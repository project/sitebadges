<?php

/**
 * Admin callbacks for the sitebadges module.
 *
 * @file: sitebadges.admin.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * sitebadges_settings - return the system settings form
 */
function sitebadges_settings() {
  $form = array();

  $options = array();
  $res = db_query("SELECT presetid,presetname FROM {imagecache_preset}");
  while ($preset = db_fetch_object($res)) {
    $options[$preset->presetid] = check_plain($preset->presetname);
  }

  $form['sitebadges_imagecache_presets'] = array(
    '#type' => 'select',
    '#title' => t('Imagecache Presets'),
    '#options' => $options,
    '#default_value' => variable_get('sitebadges_imagecache_presets', array()),
    '#description' => t('Select the imagecache presets that you want to use with site badge iamges'),
    '#required' => TRUE,
    '#multiple' => TRUE,
  );
  $form['add_another'] = array(
    '#type' => 'markup',
    '#value' => '<div>' . l(t('Add another sitebadge imagecache preset'), 'admin/settings/sitebadges/add_preset') . '</div>',
  );

  return system_settings_form($form);
}

/**
 * sitebadges_add_preset - return the form to add
 *  a imagecache preset.
 */
function sitebadges_add_preset() {
  $form = array();

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#required' => TRUE,
  );
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), '/admin/settings/sitebadges'),
  );

  return $form;
}

function sitebadges_add_preset_validate($form, &$form_state) {
  $res = db_result(db_query(
    "SELECT COUNT(*) FROM {imagecache_preset} WHERE presetname='%s'", 
    'sitebadges-' . $form_state['values']['width'] . 'x' . $form_state['values']['height']
  ));
  if ($res) {
    form_set_error('width', t('You already have a preset for this size, choose a different width or height'));
  }
  if (!is_numeric($form_state['values']['width'])) {
    form_set_error('width', t('You must enter a numeric value for the width'));
  }
  if (!is_numeric($form_state['values']['height'])) {
    form_set_error('height', t('You must enter a numeric value for the height'));
  }
}

function sitebadges_add_preset_submit($form, &$form_state) {
  $preset = new stdClass();
  $preset->presetname = 'sitebadges-' . $form_state['values']['width'] . 'x' . $form_state['values']['height'];
  drupal_write_record('imagecache_preset', $preset);
  $presets[] = $preset->presetid;
  $action = new stdClass();
  $action->presetid = $preset->presetid;
  $action->module = 'imagecache';
  $action->action = 'imagecache_resize';
  $action->data = array('width' => $form_state['values']['width'], 'height' => $form_state['values']['height']);
  drupal_write_record('imagecache_action', $action);

  cache_clear_all('imagecache:presets', 'cache');

  // Clear the content.module cache (refreshes the list of formatters provided by imagefield.module).
  if (module_exists('content')) {
    content_clear_type_cache();
  }

  $presets = variable_get('sitebadges_imagecache_presets', array());
  $presets[] = $preset->presetid;
  variable_set('sitebadges_imagecache_presets', $presets);

  $form_state['redirect'] = 'admin/settings/sitebadges';
}

/**
 * sitebadges_delete_preset - remove deleted presets from
 *  the list of available presets.
 *
 * TODO - make sure to update any nodes that use this preset too...
 */
function sitebadges_delete_preset($form, &$form_state) {
  $new_presets = array();
  $presets = variable_get('sitebadges_imagecache_presets', array());

  foreach ($presets as $preset) {
    if ($preset != $form['presetid']['#value']) {
      $new_presets[] = $preset;
    }
  }
  
  variable_set('sitebadges_imagecache_presets', $new_presets);
}

