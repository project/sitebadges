
/**
 * JavaScript to load the embed code textarea
 *  with AHAH.
 *
 * @file: sitebadges.js
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

Drupal.behaviors.getcode_expand = function(context) {
  $('.sitebadges-getcode:not(sitebadges-getcode-processed)', context).addClass('sitebadges-getcode-processed').click(function() {
    var nid_ary = this.id.split('_');
    var nid = nid_ary[1];

    $(this).after('<div class="ahah-progress ahah-progress-' + nid + '"><div class="throbber"></div></div>');
    $(this).before('<div class="sitebadges-getcode-wrapper" id="sitebadges-getcode-wrapper_' + nid + '"></div>');
    $('#sitebadges-getcode-wrapper_' + nid).hide();
    path = Drupal.settings.basePath + 'sitebadges/js/getcode/' + nid;
    $.getJSON(
      path,
      function(json) {
        $('.ahah-progress-' + nid).empty();
        $('#sitebadges-getcode_' + nid).hide();
        $('#sitebadges-getcode-wrapper_' + nid).html(json.data);
        $('#sitebadges-getcode-wrapper_' + nid).slideDown('normal');
        Drupal.attachBehaviors('#sitebadges-getcode-wrapper_' + nid);
      }
    );
    return false; // prevent nav
  });
}

Drupal.behaviors.getcode_collapse = function(context) {
  $('.sitebadges-getcode-done:not(sitebadges-getcode-done-processed)', context).addClass('sitebadges-getcode-done-processed').click(function() {
    var nid_ary = this.id.split('_');
    var nid = nid_ary[1];

    $('#sitebadges-getcode-wrapper_' + nid).slideUp('normal');
    $('#sitebadges-getcode-wrapper_' + nid).empty();
    $('#sitebadges-getcode_' + nid).show();
    return false; // prevent nav
  });
}

