<?php

/**
 * JavaScript callbacks for the sitebadges module. 
 *
 * @file: sitebadges.js.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009 
 */

/**
 * sitebadges_js_getcode - return the textarea
 *  for the sitebadge.
 */
function sitebadges_js_getcode($nid) {
  // for some reason boost wants to cache our pages regardless
  // of what we tell it to do, so be pretty blunt about what
  // we really want
  if (module_exists('boost')) {
    $GLOBALS['_boost_cache_this'] = FALSE;
  }

  $result = array(
    'status' => FALSE,
    'data' => t('There was a problem getting the embed code for this badge, please try again'),
  );
  $sitebadge = node_load($nid);

  if ($sitebadge->nid) {
    module_load_include('inc', 'sitebadges', 'sitebadges.pages');
    $result['status'] = TRUE;
    $result['data'] = drupal_get_form('sitebadges_getcode_form', $sitebadge);
  }

  drupal_json($result);
}
 
