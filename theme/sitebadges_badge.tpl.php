<?php

/**
 * Theme template for individual sitebadges.
 *
 * @file: sitebadges_badge.tpl.php
 * @author: Elliott Foster
 * @copyright: NewMBC 2009 
 *
 * AVAILABLE VARIABLES:
 *  $node - the full node object
 *  $link - the url for the badge
 *  $file - the filepath for the badge image
 *  $sitebadge - the themed badge image/link
 *  $getcode - the link to the embed code - NOTE:
 *    this variable will ONLY be set if the user
 *    has access to embed codes
 */
?>
<div class="sitebadge">

  <?php print $sitebadge ?>

</div>
<div class="getcode">

  <?php print $getcode ?>

</div>
