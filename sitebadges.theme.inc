<?php

/**
 * Theme callbacks for the sitebadges module.
 *
 * @file: sitebadges.theme.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * Implementation of module_preprocess_sitebadges_badge()
 */
function sitebadges_preprocess_sitebadges_badge(&$vars) {
  global $user;

  $vars['imagecache_preset'] = imagecache_preset($vars['node']->presetid);
  $vars['sitebadge'] = l(
    theme('imagecache', $vars['imagecache_preset']['presetname'], $vars['file']),
    $vars['link'],
    array('html' => TRUE)
  );

  // add the link to get the sitebadge code if the user
  // has access to it
  if ($user->uid == $vars['node']->uid || user_access('get sitebage embed codes')) {
    $vars['getcode'] = l(
      t('Get the embed code'), 
      'sitebadges/getcode/' . $vars['node']->nid, 
      array(
        'attributes' => array(
          'class' => 'sitebadges-getcode',
          'id' => 'sitebadges-getcode_' . $vars['node']->nid
        )
      )
    );
  }
}

