<?php

/**
 * Page callbacks for the sitebadges module.
 *
 * @file: sitebadges.pages.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * sitebadges_getcode - return the embed code
 *  for a given sitebadge.
 */
function sitebadges_getcode($nid) {
  $out = '';
  $sitebadge = node_load($nid);

  if ($sitebadge->nid) {
    $out = '<div class="about-code">' . t('Copy and paste the following HTML to embed the badge on other sites') . '</div>';
    $out .= drupal_get_form('sitebadges_getcode_form', $sitebadge);
  }
  else {
    drupal_not_found();
    return;
  }

  return $out;
}

function sitebadges_getcode_form($form_state, $sitebadge) {
  $form = array();

  $preset = imagecache_preset($sitebadge->presetid);

  $form['code'] = array(
    '#type' => 'textarea',
    '#default_value' => l(
      theme('imagecache', $preset['presetname'], $sitebadge->badge_file->filepath),
      $sitebadge->link,
      array('html' => TRUE)
    ),
  );
  $form['done'] = array(
    '#type' => 'markup',
    '#value' => l(t('Click here when done'), 'node/' . $sitebadge->nid, array('attributes' => array('class' => 'sitebadges-getcode-done', 'id' => 'sitebadges-getcode-done_' . $sitebadge->nid))),
  );

  return $form;
}

